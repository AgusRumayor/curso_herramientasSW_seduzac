# Día 1
* Actividades previas
* Actualmente, ¿cómo programo?
* Git en un día
* Otras herramientas de colaboración

---
# Día 2
* Python, ¿Por qué aprender más lenguajes?
* Micro-frameworks

---
# Día 3
* Un proyecto moderno de software
* Testing (postman)
* Coding (git, editores...)
* Deploying (Gitlab, cloud)

---
# Actividades previas
* Cuenta [gitlab](https://gitlab.com)
* Cuenta [codetasty](https://codetasty.com)
* Cuenta [hackerrank](https://hackerrank.com)
* [Encuesta](https://docs.google.com/forms/d/e/1FAIpQLSfexVDwDLSBJtvsogKA1RnqiZ0_cFEetWc3VhsgKDZ2nByUew/viewform)

---
# Actualmente, ¿Cómo programo?

---
# Git en un día

[Introducción a Git](https://carloscarcamo.gitbooks.io/git-intro/content/https://carloscarcamo.gitbooks.io/git-intro/content/)